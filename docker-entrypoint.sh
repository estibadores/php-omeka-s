#!/bin/bash

# CONFIGURE DATABASE
set -e;

if [[ -z "$MYSQL_USER" ]]; then MYSQL_USER="omeka"; fi
if [[ -z "$MYSQL_PASSWORD" ]]; then MYSQL_PASSWORD="omeka"; fi
if [[ -z "$MYSQL_DATABASE" ]]; then MYSQL_DATABASE="omeka"; fi
if [[ -z "$MYSQL_HOST" ]]; then MYSQL_HOST="db"; fi

printf 'user = "%s"\npassword = "%s"\ndbname = "%s"\nhost = "%s"\n' $MYSQL_USER $MYSQL_PASSWORD $MYSQL_DATABASE $MYSQL_HOST > /sindominio/omeka-s/config/database.ini

exec "$@"
