FROM registry.sindominio.net/debian as builder

# Install dependencies
ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && apt-get install -y git imagemagick wget unzip

RUN apt-get update && \
    apt-get -qy install \
    php-fpm php-mysql php-curl php-gd php-mbstring php-xml php-xmlrpc php-soap php-intl php-zip php-imagick\
    ghostscript wget sudo curl openssl imagemagick\
		poppler-utils && \
    apt-get clean

RUN useradd omeka -d /sindominio/

COPY omeka.conf /etc/php/7.3/fpm/pool.d/www.conf
RUN mkdir /run/php/
RUN mkdir logs

# TODO: Change Policy for PDF convert - https://stackoverflow.com/questions/52998331/imagemagick-security-policy-pdf-blocking-conversion

RUN curl -sL https://deb.nodesource.com/setup_14.x -o nodesource_setup.sh
RUN bash nodesource_setup.sh
RUN apt-get install -y nodejs


# Install omeka

WORKDIR sindominio
RUN chown omeka:omeka /sindominio

USER omeka

RUN git clone --recursive https://github.com/omeka/omeka-s.git
RUN chmod 775 omeka-s

WORKDIR /sindominio/omeka-s

RUN npm install
RUN npx gulp init

# Install modules Nov 2020

# oai pmh module
RUN wget https://github.com/Daniel-KM/Omeka-S-module-OaiPmhRepository/releases/download/3.3.5/OaiPmhRepository-3.3.5.zip -O /tmp/oaipmh.zip
RUN unzip /tmp/oaipmh.zip -d modules/

# import from omeka 2
RUN wget https://github.com/omeka-s-modules/Omeka2Importer/releases/download/v1.4.0/Omeka2Importer-1.4.0.zip -O /tmp/importer.zip
RUN unzip /tmp/importer.zip -d modules/

# bulk edition
RUN wget https://github.com/Daniel-KM/Omeka-S-module-BulkEdit/releases/download/3.3.12.4/BulkEdit-3.3.12.4.zip -O /tmp/bulkedit.zip
RUN unzip /tmp/bulkedit.zip -d modules/

# custom vocab
RUN wget https://github.com/omeka-s-modules/CustomVocab/releases/download/v1.3.1/CustomVocab-1.3.1.zip -O /tmp/customvocab.zip
RUN unzip /tmp/customvocab.zip -d modules/

# extract pdf content to text field
RUN wget https://github.com/omeka-s-modules/ExtractText/releases/download/v1.2.0/ExtractText-1.2.0.zip -O /tmp/extract.zip
RUN unzip /tmp/extract.zip -d modules/

# metadata links
RUN wget https://github.com/omeka-s-modules/MetadataBrowse/releases/download/v1.4.0/MetadataBrowse-1.4.0.zip -O /tmp/metadata.zip
RUN unzip /tmp/metadata.zip -d modules/

# TODO: bulk check - https://gitlab.com/Daniel-KM/Omeka-S-module-BulkCheck - depend Logs & Generic

RUN rm -rf /tmp/*.zip

USER root

RUN apt-get purge -qy nodejs git curl wget openssl
RUN apt-get -qy clean

COPY docker-entrypoint.sh /usr/local/bin/docker-entrypoint.sh

EXPOSE 9000
VOLUME /sindominio/

ENTRYPOINT ["/bin/bash","/usr/local/bin/docker-entrypoint.sh"]

CMD ["/usr/sbin/php-fpm7.3","--nodaemonize"]]
